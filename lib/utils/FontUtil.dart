class FontUtil{

   double screenWidth;
   FontUtil({this.screenWidth});

    double verySmall(){
     if(screenWidth<=320.0)
       return 8;
     else
       return 12;
   }

   double small(){
     if(screenWidth<=320.0)
       return 12;
     else
       return 14;
   }

   double medium(){
     if(screenWidth<=320.0)
       return 13;
     else
       return 18;
   }

   double large(){
     if(screenWidth<=320.0)
       return 20;
     else
       return 24;
   }

   double largest(){
     if(screenWidth<=320.0)
       return 23;
     else
       return 30;
   }


}
