import 'dart:ui';

import 'package:flutter/material.dart';

class TextUtils{
  static const Color greyBackground=Color(0xffF2F2F2);
  static const Color textGrey=Color(0xff9C9C9C);
  static const Color mediumGrey = Color(0xff828282);
  static const Color blackText = Color(0xff353635);
  static const Color lightBlackText = Color(0xff504D4D);
  static const Color underlineGreen = Color(0xff92B45C);
  static const Color primaryDarkColor = Color(0xff58693D);
  static const Color headingGrey = Color(0xff959595);
  static const int _greenPrimaryValue = 0xFF607B35;
  static const Color buttonPrimaryColor = Color(0xff607B35);
  static const Color dateItemGreen = Color(0xff899576);
  static const MaterialColor green = MaterialColor(
    _greenPrimaryValue,
    <int, Color>{
      50: Color(0xFFE8F5E9),
      100: Color(0xFFC8E6C9),
      200: Color(0xFF92B45C),
      300: Color(0xFF607B35),
      400: Color(0xFF607B35),
      500: Color(_greenPrimaryValue),
      600: Color(0xFF607B35),
      700: Color(0xFF58693D),
      800: Color(0xFF58693D),
      900: Color(0xFF58693D),
    },
  );
}