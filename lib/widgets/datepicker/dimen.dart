class Dimen {
  Dimen._();

  static const double dateTextSize = 24;
  static const double dayTextSize = 13;
  static const double monthTextSize = 13;
}
