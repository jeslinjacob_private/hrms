
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:hrms/utils/TextUtils.dart';
import 'dimen.dart';

 const TextStyle defaultMonthTextStyle = TextStyle(
  color: TextUtils.dateItemGreen,
  fontSize: Dimen.monthTextSize,
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w400,
);

 const TextStyle defaultDateTextStyle = TextStyle(
    color: TextUtils.dateItemGreen,
    fontSize: Dimen.dateTextSize,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

 const TextStyle defaultDayTextStyle = TextStyle(
  color: TextUtils.dateItemGreen,
  fontSize: Dimen.dayTextSize,
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w400,
);

const TextStyle selectedDayTextStyle = TextStyle(
  color: Colors.white,
  fontSize: Dimen.dayTextSize,
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w400,
);
const TextStyle selectedMonthTextStyle = TextStyle(
  color: Colors.white,
  fontSize: Dimen.monthTextSize,
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w400,
);
const TextStyle selectedDateTextStyle = TextStyle(
  color: Colors.white,
  fontSize: Dimen.dateTextSize,
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w500,
);