import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:hrms/utils/FontUtil.dart';
import 'package:hrms/utils/TextUtils.dart';


class ResponsiveStyle{
  double screenWidth;
  ResponsiveStyle({this.screenWidth});

 TextStyle defaultMonthTextStyle()=> TextStyle(
      color: TextUtils.dateItemGreen,
      fontSize: FontUtil(screenWidth: screenWidth).small(),
      fontFamily: 'Roboto',
      fontWeight: FontWeight.w400,
    );

  TextStyle defaultDateTextStyle() => TextStyle(
    color: TextUtils.dateItemGreen,
    fontSize: FontUtil(screenWidth: screenWidth).medium(),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

  TextStyle defaultDayTextStyle() => TextStyle(
    color: TextUtils.dateItemGreen,
    fontSize: FontUtil(screenWidth: screenWidth).small(),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w400,
  );


  TextStyle selectedDayTextStyle() => TextStyle(
    color: Colors.white,
    fontSize: FontUtil(screenWidth: screenWidth).small(),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w400,
  );
  TextStyle selectedMonthTextStyle() => TextStyle(
    color: Colors.white,
    fontSize: FontUtil(screenWidth: screenWidth).small(),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w400,
  );
  TextStyle selectedDateTextStyle() => TextStyle(
    color: Colors.white,
    fontSize: FontUtil(screenWidth: screenWidth).medium(),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );
}