import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hrms/utils/TextUtils.dart';
import 'CustomStyleSelector.dart';
import 'DateWidget.dart';
import 'style.dart';
import 'tap.dart';

class HorizontalDatePicker extends StatefulWidget {

  double width;
  double height;
  TextStyle monthTextStyle, dayTextStyle, dateTextStyle;
  Color selectionColor;
  DateTime currentDate;
  DateChangeListener onDateChange;
  int daysCount;
  double screenWidth;
  String locale;

  HorizontalDatePicker(
      this.currentDate, {
        Key key,
        this.width,
        this.height = 80,
        this.monthTextStyle = defaultMonthTextStyle,
        this.dayTextStyle = defaultDayTextStyle,
        this.dateTextStyle = defaultDateTextStyle,
        this.selectionColor = TextUtils.primaryDarkColor,
        this.daysCount,
        this.onDateChange,
        this.locale,
        this.screenWidth
      }
  ): super(key: key);

  @override
  State<StatefulWidget> createState() => new _DatePickerState();
}

class _DatePickerState extends State<HorizontalDatePicker>{

  ScrollController _controller = ScrollController();
  bool flagController = true;

  @override
  Widget build(BuildContext context) {
//
//
    if(flagController){
      flagController=false;

      /**
       * this timer is for smoothly moving to the current date when the calnder is loaded
       * it takes some time  for the controller to set into ListViewBuilder
       * and only after it is set we can access maxScrollEven value
       * then we do calculation for finding the pixel to display when the calander initially loads up with current date
       */
      Timer(Duration(milliseconds: 500), () {
        var maxVal = _controller.position.maxScrollExtent;
        double selectedPixel = (maxVal / widget.daysCount)* widget.currentDate.day;
        _controller.animateTo(selectedPixel,curve: Curves.linear, duration: Duration (milliseconds: 500));

      });
    }
    return Container(
      width: widget.width,
      height: widget.height,
      child: ListView.builder(
        controller: _controller,
        itemCount: widget.daysCount,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          // Return the Date Widget
          DateTime _date = DateTime.now();
          DateTime _monthStart = new DateTime(_date.year,_date.month,01).add(Duration(days: index));
          DateTime date = new DateTime(_monthStart.year, _monthStart.month, _monthStart.day);
          bool isSelected = compareDate(date, widget.currentDate);

          return DateWidget(
            date: date,
            monthTextStyle: isSelected ? ResponsiveStyle(screenWidth: widget.screenWidth).selectedMonthTextStyle() :ResponsiveStyle(screenWidth: widget.screenWidth).defaultMonthTextStyle(),
            dateTextStyle: isSelected ? ResponsiveStyle(screenWidth: widget.screenWidth).selectedDateTextStyle() :ResponsiveStyle(screenWidth: widget.screenWidth).defaultDateTextStyle(),
            dayTextStyle: isSelected ? ResponsiveStyle(screenWidth: widget.screenWidth).selectedDayTextStyle() :ResponsiveStyle(screenWidth: widget.screenWidth).defaultDayTextStyle(),
            locale: widget.locale,
            selectionColor:
            isSelected ? widget.selectionColor : Colors.transparent,
            onDateSelected: (selectedDate) {
              // A date is selected
              if (widget.onDateChange != null) {
                widget.onDateChange(selectedDate);
              }
              setState(() {
                widget.currentDate = selectedDate;
              });
            },
          );
        },
      ),
    );
  }

  bool compareDate(DateTime date1, DateTime date2) {
    return date1.day == date2.day &&
        date1.month == date2.month &&
        date1.year == date2.year;
  }
}
