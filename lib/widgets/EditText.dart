import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hrms/utils/TextUtils.dart';

class EditText extends StatelessWidget {

  final String hint;
  String textData;
  EditText({this.hint});

  @override
  Widget build(BuildContext context) {
    return Container(

      margin: EdgeInsets.only(left: 30 , right: 30 , top: 10),

      child: TextField(
        style: new TextStyle(color: Colors.black , fontSize: 12 , fontFamily: 'Roboto' ,fontWeight: FontWeight.w400),
        decoration: InputDecoration(
//            contentPadding: EdgeInsets.only(left: 30 , right: 10 , top: 15 , bottom: 15),
            contentPadding: EdgeInsets.only(left: 30 , right: 10 , top: 8 , bottom: 8),
            hintText: hint,
            border: new OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(
                width: 0,
                style: BorderStyle.none,
              ),
            ),
            filled: true,
            hintStyle: new TextStyle(color: TextUtils.textGrey , fontSize: 12 , fontFamily: 'Roboto' ,fontWeight: FontWeight.w400),
            fillColor: TextUtils.greyBackground),
            onChanged: (text){ textData = text; },
      ),
    );
  }

  String get getTextData{
    return textData;
  }
}
