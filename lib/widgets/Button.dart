import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hrms/utils/TextUtils.dart';

class Button extends StatelessWidget {

  Button({ @required this.onPressed , this.text ,this.top=0,this.bottom=0,this.right=0,this.left=0,this.screenWidth});

  final GestureTapCallback onPressed;
  double top, bottom,right,left,screenWidth;
  String text;
  @override
  Widget build(BuildContext context) {
    {
      return Container(
        margin: EdgeInsets.only(left: left , right: right , bottom: bottom , top: top),

        child: RawMaterialButton(
          fillColor: TextUtils.buttonPrimaryColor,


          child: Padding(
            padding: screenWidth<=320 ? const EdgeInsets.only(top: 10,bottom: 10) : const EdgeInsets.only(top: 15,bottom: 15) ,
            child: Center(

              child: Text(text , style: TextStyle(color: Colors.white , fontFamily: 'Roboto' , fontWeight: FontWeight.w400 , fontSize: 15),),
            ),
          ),
          onPressed: onPressed,
          shape: new OutlineInputBorder(
              borderRadius: BorderRadius.circular(10) ,
              borderSide: BorderSide(
                width: 0,
                style: BorderStyle.none,
          )),
        ),
      );
    }
  }
}
