import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hrms/utils/FontUtil.dart';
import 'package:hrms/utils/TextUtils.dart';

import 'TextView.dart';

class DrawerItem extends StatelessWidget {

  String icon,text;
  double marginTop,screenWidth;
  FontStyle fontStyle;
  GestureTapCallback onPress;
  DrawerItem({this.text,this.icon, this.marginTop=15,this.fontStyle=FontStyle.regular,this.onPress,this.screenWidth});


  @override
  Widget build(BuildContext context) {
    return  Container(
        margin: EdgeInsets.only(left: 30,top: marginTop),
        child:InkWell(
          onTap: (){onPress();},
          child: Row(

              children: <Widget>[
                Image.asset(icon , width: 23 , height: 23,),
                TextView(text: this.text,color: TextUtils.textGrey,fontSize: FontUtil(screenWidth: screenWidth).large(),fontStyle: fontStyle,marginLeft: 10,)
              ],
            ),
        )
        );

  }
}
