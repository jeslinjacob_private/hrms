import 'package:flutter/cupertino.dart';

class Logo extends StatelessWidget {

  String src;
  double width,height,marginTop,marginBottom;

//  Logo({this.src,this.width,this.height,[this.marginTop=10]});
  Logo({String src,double width, double height , double marginTop = 10, double marginBottom = 5}){
    this.src = src;
    this.width= width;
    this.height=height;
    this.marginBottom=marginBottom;
    this.marginTop = marginTop;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
//      decoration: BoxDecoration( border: Border.all(color: Colors.lightBlue)),
      margin: EdgeInsets.only(top: marginTop , bottom: marginBottom),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
//          Image.asset('images/logohrms.png', width: 300,height: 100,)
          Image.asset(src, width: this.width,height: this.height,)
        ],
      ),
    );
  }
}
