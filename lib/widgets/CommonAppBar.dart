import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hrms/utils/TextUtils.dart';
import 'package:hrms/widgets/TextView.dart';

class CommonAppBar extends StatelessWidget {

  CommonAppBar({ @required this.onDrawerIconClicked });
  final GestureTapCallback onDrawerIconClicked;
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: Colors.white ,boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.grey,
              blurRadius: 15.0,
              offset: Offset(0.0, 0.19)
          )
        ], ),
        height: 100,
        padding: EdgeInsets.only(top: 20),
        child: Row(
          children: <Widget>[
            IconButton(
              icon: new Image.asset('images/menueIcon.png'),
              onPressed: () { onDrawerIconClicked(); }
            ),
            Image.asset("images/imageprofile.png" , width: 48 , height: 48,),
            Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TextView(text: "Marina Mcdonald",color: Colors.black,fontSize: 16,fontStyle: FontStyle.bold,),
                  TextView(text: "Emp123",color: TextUtils.textGrey,fontSize: 12,fontStyle: FontStyle.medium,)
                ],
              ),
            ),
            Spacer(),
            Container(
              margin: EdgeInsets.only(right: 10),
              child: Icon(
                Icons.settings ,
                color: TextUtils.textGrey,

              ),
            )
          ],
        ),
      );

  }
}
