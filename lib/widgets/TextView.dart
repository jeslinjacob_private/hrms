import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextView extends StatelessWidget {

  String text;
  Color color;
  double fontSize , marginLeft,marginRight,marginTop,marginBottom;
  FontWeight fontWeight;

  TextView({ this.text , this.color , this.fontSize , FontStyle fontStyle ,this.marginLeft=0 , this.marginRight =0 , this.marginTop=0 , this.marginBottom=0}){
      switch(fontStyle){

        case FontStyle.regular:
          fontWeight = FontWeight.w300;
          break;
        case FontStyle.medium:
          fontWeight = FontWeight.w400;
          break;
        case FontStyle.bold:
          fontWeight = FontWeight.w500;
          break;
      }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: marginTop,bottom: marginBottom,left: marginLeft,right: marginRight),
      child: Text(
        text ,
        style: TextStyle(
            fontFamily: 'Roboto' ,
            fontWeight: this.fontWeight ,
            color: this.color ,
            fontSize: fontSize),

      ),
    );
  }
}

enum FontStyle{
  regular,
  medium,
  bold
}
