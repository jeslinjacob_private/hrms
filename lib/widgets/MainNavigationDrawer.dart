import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hrms/app/attendance/AttendancePage.dart';
import 'package:hrms/app/request/LeaveRequest.dart';
import 'package:hrms/utils/FontUtil.dart';
import 'package:hrms/utils/NoAnimationMaterialPageRoute.dart';
import 'package:hrms/utils/TextUtils.dart';

import 'DrawerItem.dart';
import 'Logo.dart';
import 'TextView.dart';

class MainNavigationDrawer extends StatelessWidget {

  double screenWidth;
  MainNavigationDrawer({this.screenWidth});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Logo(src: "images/imageprofile.png",width: 100,height: 100 ,marginTop: 20,marginBottom: 13,),
        TextView(text: "Hello",fontStyle: FontStyle.medium,fontSize: FontUtil(screenWidth: screenWidth).medium(),color: TextUtils.textGrey,),
        TextView(text: "Marina Mcdonald",fontStyle: FontStyle.medium,fontSize: FontUtil(screenWidth: screenWidth).largest(),color: Colors.black,),
        Container(child: Divider(color: Colors.grey,), margin: EdgeInsets.only(top: 10 , left: 10 , right: 10),),
        DrawerItem(icon: "images/attendance.png",text: "Attendance",marginTop: 25,onPress: (){
          Navigator.of(context).pushReplacement( new NoAnimationMaterialPageRoute(builder: (context) => new AttendancePage()));
        },screenWidth: screenWidth,),
        DrawerItem(icon: "images/attreq.png",text: "Add leave request",onPress: (){
          Navigator.of(context).pushReplacement( new NoAnimationMaterialPageRoute(builder: (context) => new LeaveRequest()));
        },screenWidth: screenWidth),
        DrawerItem(icon: "images/viewreq.png",text: "View request",onPress: (){ print("hai 3"); },screenWidth: screenWidth),
        DrawerItem(icon: "images/verified.png",text: "Leave balance",onPress: (){ print("hai 4"); },screenWidth: screenWidth),
        DrawerItem(icon: "images/logout.png",text: "Logout",marginTop: 60,fontStyle: FontStyle.bold,onPress: (){ print("hai 5");},screenWidth: screenWidth)
      ],
    );
  }
}
