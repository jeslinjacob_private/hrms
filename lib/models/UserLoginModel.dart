class UserAuthModel {
  final String clientCode = "MHR";
  final String methodName = "validateUserStatus";
  String password;
  String userName;

  UserAuthModel(
      {this.userName ,this.password});

  Map<String, dynamic> toJsonAuth() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ClientCode'] = this.clientCode;
    data['MethodName'] = this.methodName;
    data['Password'] = this.password;
    data['UserName'] = this.userName;
    return data;
  }

  Map<String, dynamic> toJsonLogin(String token,String clientId,String methodName) {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ClientCode'] = this.clientCode;
    data['MethodName'] = methodName;
    data['Password'] = this.password;
    data['UserName'] = this.userName;
    data['IsMob'] = 1;
    data['Token'] = token;
    data['ClientId']=clientId;
    return data;
  }
}

// ignore: slash_for_doc_comments
/**
 * this is the class for getting value from the response of login
 * API call
 */

class UserLoginResponse {
  List<Response> response;
  List<ResultSet> resultSet;

  UserLoginResponse({this.response, this.resultSet});

  UserLoginResponse.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<Response>();
      json['Response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
    if (json['ResultSet'] != null) {
      resultSet = new List<ResultSet>();
      json['ResultSet'].forEach((v) {
        resultSet.add(new ResultSet.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    if (this.resultSet != null) {
      data['ResultSet'] = this.resultSet.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Response {
  String eRRORCODE;
  String eRRORMSG;

  Response({this.eRRORCODE, this.eRRORMSG});

  Response.fromJson(Map<String, dynamic> json) {
    eRRORCODE = json['ERROR_CODE'];
    eRRORMSG = json['ERROR_MSG'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ERROR_CODE'] = this.eRRORCODE;
    data['ERROR_MSG'] = this.eRRORMSG;
    return data;
  }
}

class ResultSet {
double uSERID;
String cLIENTCODE;
String bLOCKEDTILL;
int fAILUREATTEMPTS;
String tOKEN;
String clientId;

ResultSet(
    {this.uSERID,
      this.cLIENTCODE,
      this.bLOCKEDTILL,
      this.fAILUREATTEMPTS,
      this.tOKEN,
      this.clientId});

ResultSet.fromJson(Map<String, dynamic> json) {
uSERID = json['USER_ID'];
cLIENTCODE = json['CLIENT_CODE'];
bLOCKEDTILL = json['BLOCKED_TILL'];
fAILUREATTEMPTS = json['FAILURE_ATTEMPTS'];
tOKEN = json['TOKEN'];
clientId = json['ClientId'];
}

Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['USER_ID'] = this.uSERID;
  data['CLIENT_CODE'] = this.cLIENTCODE;
  data['BLOCKED_TILL'] = this.bLOCKEDTILL;
  data['FAILURE_ATTEMPTS'] = this.fAILUREATTEMPTS;
  data['TOKEN'] = this.tOKEN;
  data['ClientId'] = this.clientId;
  return data;
}
}