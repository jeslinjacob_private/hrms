import 'UserLoginModel.dart';

class UserAccountDetails {
  List<Response> response;
  List<ResultSet> resultSet;

  UserAccountDetails({this.response, this.resultSet});

  UserAccountDetails.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<Response>();
      json['Response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
    if (json['ResultSet'] != null) {
      resultSet = new List<ResultSet>();
      json['ResultSet'].forEach((v) {
        resultSet.add(new ResultSet.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    if (this.resultSet != null) {
      data['ResultSet'] = this.resultSet.map((v) => v.toJson()).toList();
    }
    return data;
  }
}



class ResultSet {
  double sessionId;
  String empName;
  double userId;
  double empId;
  double companyId;
  String companyName;
  double orgUnitId;
  String unitCode;
  String unitName;
  String loginDate;
  String empCode;

  ResultSet(
      {this.sessionId,
        this.empName,
        this.userId,
        this.empId,
        this.companyId,
        this.companyName,
        this.orgUnitId,
        this.unitCode,
        this.unitName,
        this.loginDate,
        this.empCode});

  ResultSet.fromJson(Map<String, dynamic> json) {
    sessionId = json['SESSION_ID'];
    empName = json['EMP_NAME'];
    userId = json['USER_ID'];
    empId = json['EMP_ID'];
    companyId = json['COMPANY_ID'];
    companyName = json['COMPANY_NAME'];
    orgUnitId = json['ORG_UNIT_ID'];
    unitCode = json['UNIT_CODE'];
    unitName = json['UNIT_NAME'];
    loginDate = json['LOGIN_DATE'];
    empCode = json['EMP_CODE'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['SESSION_ID'] = this.sessionId;
    data['EMP_NAME'] = this.empName;
    data['USER_ID'] = this.userId;
    data['EMP_ID'] = this.empId;
    data['COMPANY_ID'] = this.companyId;
    data['COMPANY_NAME'] = this.companyName;
    data['ORG_UNIT_ID'] = this.orgUnitId;
    data['UNIT_CODE'] = this.unitCode;
    data['UNIT_NAME'] = this.unitName;
    data['LOGIN_DATE'] = this.loginDate;
    data['EMP_CODE'] = this.empCode;
    return data;
  }
}