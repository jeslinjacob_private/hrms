import 'package:flutter/material.dart';
import 'package:hrms/network/post_api_service.dart';
import 'package:hrms/utils/TextUtils.dart';
import 'package:provider/provider.dart';
import 'package:logging/logging.dart';

import 'SplashScreen.dart';


void main() {
  _setupLogging();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return Provider(
      builder: (_)=>PostApiService.create(),
      dispose: (context,PostApiService service)=>service.client.dispose(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: TextUtils.green,
        ),
        debugShowCheckedModeBanner: false,
        home: SplashScreen(),
      ),
    );
  }

}
void _setupLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((rec) {
    print('${rec.level.name}: ${rec.time}: ${rec.message}');
  });
}


