import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hrms/app/attendance/AttendancePage.dart';
import 'package:hrms/app/login_module/LoginPresenter.dart';
import 'package:hrms/widgets/Button.dart';
import 'package:hrms/widgets/EditText.dart';
import 'package:hrms/widgets/Logo.dart';
import '../../utils/TextUtils.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> implements MvpLogin{

  Size screeSize;
  /// widget declarations should be written here
  EditText _mUsernameTxt = new EditText( hint: 'username');
  EditText _mPassword=new EditText(hint: 'password');
  LoginPresenter _mPresenter;

  /// build UI here
  @override
  Widget build(BuildContext context) {

    screeSize = MediaQuery.of(context).size;
    _mPresenter = new LoginPresenter(this);


    return Scaffold(
      backgroundColor: TextUtils.greyBackground,
//      resizeToAvoidBottomPadding: true,
      body: Center(
        child: Card(
          margin: EdgeInsets.only(left: 20 , right: 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Logo(src: "images/logohrms.png", width: screeSize.width/1.5,height: 100,),
              _mUsernameTxt,
              _mPassword,
              Button(
                text: 'LOGIN',left: 30 , right: 30 , bottom: 20 , top: 20,
                onPressed: () {

                _mPresenter.getLoginResponse(context, 'mf33606', 'hrms1234');
                },
                screenWidth: screeSize.width,
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void showToast(String errorMsg) {
    Fluttertoast.showToast(
        msg: errorMsg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.grey,
        textColor: Colors.white,
        fontSize: 14.0
    );
//    Navigator.push(context,MaterialPageRoute(builder: (context) => AttendancePage()));
  }

  @override
  void loginSuccess() {

    Navigator.of(context).pushReplacement( new MaterialPageRoute(builder: (context) => new AttendancePage()));
//    Navigator.push(context,MaterialPageRoute(builder: (context) => AttendancePage()));

  }




}
