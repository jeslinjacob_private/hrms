import 'package:flutter/cupertino.dart';
import 'package:hrms/models/UserAccountDetails.dart';
import 'package:hrms/models/UserLoginModel.dart';
import 'package:hrms/network/post_api_service.dart';
import 'package:hrms/utils/StringConstants.dart';
import 'package:provider/provider.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class LoginPresenter {
  MvpLogin _loginInterface;

  LoginPresenter(MvpLogin interface) {
    _loginInterface = interface;
  }

  void getLoginResponse(BuildContext context , String username , String pass) async{


    final response = await Provider.of<PostApiService>(context)
        .login(new UserAuthModel(userName: username, password: pass).toJsonAuth());


        UserLoginResponse auth = UserLoginResponse.fromJson(response.body);


        if(auth.resultSet!=null){

          var encryptedPass = sha1.convert(utf8.encode(pass));

          final loginResponse = await Provider.of<PostApiService>(context)
              .login(new UserAuthModel(userName: username.toUpperCase(),password: encryptedPass.toString().toUpperCase()).toJsonLogin(auth.resultSet[0].tOKEN, auth.resultSet[0].clientId,"validateUserStatuses"));

          UserAccountDetails userAccountDetails = UserAccountDetails.fromJson(loginResponse.body);

          if(userAccountDetails.resultSet!=null){
              SharedPreferences pref = await SharedPreferences.getInstance();
              pref.setDouble(sSESSION_ID, userAccountDetails.resultSet[0].sessionId);
              pref.setDouble(sCOMPANY_ID, userAccountDetails.resultSet[0].companyId);
              pref.setString(sCOMPANY_NAME, userAccountDetails.resultSet[0].companyName);
              pref.setString(sEMP_CODE, userAccountDetails.resultSet[0].empCode);
              pref.setString(sEMP_NAME, userAccountDetails.resultSet[0].empCode);

              _loginInterface.loginSuccess();
          } else{
            _loginInterface.showToast(userAccountDetails.response[0].eRRORMSG);
          }
        }
        else{
          _loginInterface.showToast(auth.response[0].eRRORMSG);
        }

  }
}

abstract class MvpLogin {

  void loginSuccess();
  void showToast(String errorMsg);
}
