import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hrms/utils/FontUtil.dart';
import 'package:hrms/utils/TextUtils.dart';
import 'package:hrms/widgets/CommonAppBar.dart';
import 'package:hrms/widgets/MainNavigationDrawer.dart';
import 'package:hrms/widgets/TextView.dart';

class LeaveRequest extends StatefulWidget {
  @override
  _LeaveRequestState createState() => _LeaveRequestState();
}

class _LeaveRequestState extends State<LeaveRequest> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Size screeSize;
  FontUtil textSize;

  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        systemNavigationBarColor: Colors.transparent,
        systemNavigationBarIconBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark

    ));
    screeSize = MediaQuery.of(context).size;
    textSize = new FontUtil(screenWidth: screeSize.width);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: TextUtils.greyBackground,
        key: _scaffoldKey,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80),
          child: CommonAppBar(
            onDrawerIconClicked: (){
              _scaffoldKey.currentState.openDrawer();
            },
          ),
        ),
        drawer: Drawer(
            child: MainNavigationDrawer(screenWidth: screeSize.width,)
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextView(text: "Add Leave Request",color: TextUtils.headingGrey,fontStyle: FontStyle.regular,fontSize: textSize.large(),),
                Container(width: 45,height: 2, color: TextUtils.underlineGreen,),
                selectLeaveCard()

              ],
            ),
          ),
        ),
      ),

    );
  }

  Widget selectLeaveCard(){

    return Container(
      margin: EdgeInsets.only( top: 22),
      padding:  EdgeInsets.all(10),
      decoration: BoxDecoration(color: Colors.white , borderRadius: BorderRadius.circular(9.0)),
      child: Column(

        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          TextView(text: "Select Leave Type" ,color: TextUtils.lightBlackText,fontSize: 16,fontStyle:FontStyle.bold,),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Icon( Icons.arrow_back_ios , color: TextUtils.mediumGrey,size: FontUtil(screenWidth: screeSize.width).small()),
              Container(
                height: 100,
                width: screeSize.width/1.4,

                child: ListView(
                  scrollDirection: Axis.horizontal,


                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.asset("images/casualLogoRect.png", width: screeSize.width/5,height: screeSize.width/5,),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.asset("images/esiLogoRect.png", width: screeSize.width/5,height: screeSize.width/5,),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.asset("images/lopLogoRect.png", width: screeSize.width/5,height: screeSize.width/5,),
                    )

                  ],
                ),
              ),
              Icon( Icons.arrow_forward_ios , color: TextUtils.mediumGrey,size: FontUtil(screenWidth: screeSize.width).small()),

            ],
          )
        ],
      ),
    );
  }

  
}
