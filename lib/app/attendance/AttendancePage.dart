import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hrms/utils/FontUtil.dart';
import 'package:hrms/utils/TextUtils.dart';
import 'package:hrms/widgets/Button.dart';
import 'package:hrms/widgets/CommonAppBar.dart';
import 'package:hrms/widgets/MainNavigationDrawer.dart';
import 'package:hrms/widgets/TextView.dart';
import 'package:hrms/widgets/datepicker/HorizontalDatePicker.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class AttendancePage extends StatefulWidget {
  @override
  _AttendancePageState createState() => _AttendancePageState();
}

class _AttendancePageState extends State<AttendancePage> {

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Size screeSize;
  String dateTime;
  DateTime now = DateTime.now();
  FontUtil textSize;

  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        systemNavigationBarColor: Colors.transparent,
        systemNavigationBarIconBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark

    ));

    dateTime = DateFormat(' EEEE dd MMM yyyy').format(now);

    initializeDateFormatting();
    screeSize = MediaQuery.of(context).size;
    textSize = new FontUtil(screenWidth: screeSize.width);
    print(screeSize.width);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: TextUtils.greyBackground,
        key: _scaffoldKey,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80),
          child: CommonAppBar(
            onDrawerIconClicked: (){
              _scaffoldKey.currentState.openDrawer();
            },
          ),
        ),

        drawer: Drawer(
            child: MainNavigationDrawer(screenWidth: screeSize.width,)
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextView(text: "Attendance",color: TextUtils.headingGrey,fontStyle: FontStyle.regular,fontSize: textSize.large(),),
                Container(width: 45,height: 2, color: TextUtils.underlineGreen,),
                calenderWidgetCard(),
                secondCard(),
                Button(text: "PUNCH IN",top: 24,onPressed: (){
                  print("Punched in");
                },
                screenWidth: screeSize.width,)

              ],
            ),
          ),
        ),

      ),
    );
  }

  Widget calenderWidgetCard(){

    DateTime now = new DateTime.now();
    int lastDay = new DateTime(now.year,now.month+1,0).day;
    return Container(
      margin: EdgeInsets.only( top: 22),
      padding:  EdgeInsets.only(left: 5,right: 5),
      decoration: BoxDecoration(color: Colors.white , borderRadius: BorderRadius.circular(9.0)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Icon( Icons.arrow_back_ios , color: TextUtils.mediumGrey,size: FontUtil(screenWidth: screeSize.width).small()),
          HorizontalDatePicker(

            DateTime.now(),
            width: screeSize.width/1.5,
            height: screeSize.height/7,
            onDateChange: (date) {
//                                // New date selected
//                                print(date.day.toString());
            },
            screenWidth: screeSize.width,
            daysCount: lastDay,
          ),
          Icon( Icons.arrow_forward_ios , color: TextUtils.mediumGrey, size: FontUtil(screenWidth: screeSize.width).small(),)
        ],
      ),

    );
  }

  Widget statusButtons({String text , Color color , GestureTapCallback onPressed , double marginTop}){
    return  InkWell(
      onTap: (){ onPressed();},
      child: Container(
        margin: EdgeInsets.only(top: marginTop),
        child: Center(child: TextView(text: text,color: Colors.white,fontStyle: FontStyle.medium,fontSize: textSize.small(),)),
        decoration: BoxDecoration( color: color , borderRadius: BorderRadius.circular(20)),
        padding: EdgeInsets.all(8),
        width: screeSize.width/4,
      ),
    );
  }

  Widget secondCard(){
    return Container(
      margin: EdgeInsets.only( top: 22),
      padding:  EdgeInsets.all(20),
      decoration: BoxDecoration(color: Colors.white , borderRadius: BorderRadius.circular(9.0)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(child: TextView(text: dateTime,fontStyle: FontStyle.bold,color: TextUtils.blackText,fontSize: textSize.medium(),) ),
          Container(
            margin: EdgeInsets.only(top: 18,left: 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Image.asset("images/officonred.png", width: 8,height: 8,),
                TextView(text: "In : 9:00 AM",fontStyle: FontStyle.medium,color: TextUtils.blackText,fontSize: textSize.verySmall(),marginLeft: 5,),
                Spacer(),
                Image.asset("images/officonred.png", width: 8,height: 8,),
                TextView(text: "Out : 9:00 AM",fontStyle: FontStyle.medium,color: TextUtils.blackText,fontSize: textSize.verySmall(),marginLeft: 5,),

              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Row(
              children: <Widget>[
                CircularPercentIndicator(

                  animation: true,
                  startAngle: 180,
                  radius: screeSize.width/2.3,
                  lineWidth: 9.0,
                  percent: .40,
                  center: Container(child: TextView(text: "04:24:00", fontSize: textSize.largest(),fontStyle: FontStyle.bold,color: TextUtils.blackText,)),
                  progressColor: Colors.green,
                ),
                Spacer(),
                Column(
                  children: <Widget>[
                    statusButtons(text: "Present" , color: Color(0xff49C949), marginTop: 0, onPressed: (){
                      print("Present");
                    }) ,
                    statusButtons(text: "Absent" , color: Color(0xffF21E1E), marginTop: 8 , onPressed: (){
                      print("Absent");
                    }),
                    statusButtons(text: "Holiday" , color: Color(0xffF59416), marginTop: 8 , onPressed: (){
                      print("Holiday");
                    })

                  ],
                ),


              ],
            ),
          ),

        ],
      ),
    );
  }
}
