import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hrms/app/login_module/LoginPage.dart';
import 'package:hrms/widgets/Logo.dart';

import 'SplashPresenter.dart';
import 'attendance/AttendancePage.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> implements MvpSplash {

  SplashPresenter presenter;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(
    Duration(seconds: 3),()=>
        presenter.checkLoggedInStatus() );
  }
  @override
  Widget build(BuildContext context) {
    Size screeSize = MediaQuery.of(context).size;
    presenter = SplashPresenter(this);

    return Scaffold(
      body: Center(
        child: Logo(src: "images/logohrms.png" , width: screeSize.width/1.5,),
      ),
    );
  }

  @override
  void goToAttendance() {
    Navigator.of(context).pushReplacement( new MaterialPageRoute(builder: (context) => new AttendancePage()));
  }

  @override
  void goToLogin() {
    Navigator.of(context).pushReplacement( new MaterialPageRoute(builder: (context) => new LoginPage()));
  }
}
