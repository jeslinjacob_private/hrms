import 'package:hrms/utils/StringConstants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashPresenter{

  MvpSplash _mvpSplash;
  SplashPresenter(MvpSplash context){
    _mvpSplash = context;
  }

  void checkLoggedInStatus() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getDouble(sSESSION_ID)??null;
    if(sessionId!=null)
      _mvpSplash.goToAttendance();
    else
      _mvpSplash.goToLogin();
  }
}

abstract class MvpSplash{
  void goToLogin();
  void goToAttendance();
}