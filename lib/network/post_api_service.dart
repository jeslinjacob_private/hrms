import 'package:chopper/chopper.dart';
part 'post_api_service.chopper.dart';

@ChopperApi(baseUrl: 'http://59.145.109.140:8035')
abstract class PostApiService extends ChopperService{


  @Post(path: "/Retrieve.ashx/ProcessRequest")
  Future<Response> login(@Body() Map<String, dynamic> body);
//  Future<Response> authenticate(@Body() Map<String,dynamic> body);

  static PostApiService create(){
    final client = ChopperClient(
//      baseUrl: "",
      services: [
        _$PostApiService(),
      ],
      converter: JsonConverter(),
      interceptors: [
        HttpLoggingInterceptor()
      ],
    );

    return _$PostApiService(client);
  }
}